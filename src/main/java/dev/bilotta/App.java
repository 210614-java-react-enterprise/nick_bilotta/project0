package dev.bilotta;

import dev.bilotta.util.Menu;

import java.sql.*;

public class App {

    public static void main(String[] noArgs) throws SQLException {

        //call start up menu
        Menu menu = new Menu();
        menu.start();

    }


    //TODO Withdrawal and Deposit functions with validation and no overdrafts
    //Transactions will be recorded into a remote database
    //BONUS: Functionality for transferring funds between accounts


    //TODO View account balance and transaction history
    //Pull from database to view transaction history and balance (proper formatting)
    //Adhere to data persistence (3NF tables, transaction blocks in sql queries, etc)


}
