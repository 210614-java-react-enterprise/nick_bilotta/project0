package dev.bilotta.models;

import java.util.Objects;

public class Account {

    private int accountNum;
    private String type;
    private double balance;
    private int customerID;

    //noArgs constructor
    public Account() {
        super();
    }

    //Regular constructor to be used in program
    public Account(String type, int customerID) {
        this.type = type;
        this.customerID = customerID;
    }

    //All args constructor for manually adding an account number to a new account: NOT RECOMMENDED
    public Account(int accountNum, String type, double balance, int customerID) {
        this.accountNum = accountNum;
        this.type = type;
        this.balance = balance;
        this.customerID = customerID;
    }


    public int getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountNum == account.accountNum && Double.compare(account.balance, balance) == 0 && Objects.equals(type, account.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountNum, type, balance);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNum=" + accountNum +
                ", type='" + type + '\'' +
                ", balance=" + balance +
                '}';
    }

}
