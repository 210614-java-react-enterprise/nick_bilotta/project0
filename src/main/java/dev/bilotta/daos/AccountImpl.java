package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Customer;
import dev.bilotta.util.ConnectionUtil;

import java.sql.*;
import java.util.*;

public class AccountImpl implements AccountDao{


    @Override
    public List<Account> getAllAccounts() {
        return null;
    }

    @Override
    public boolean addNewAccount(Account account) {

        String sql = "insert into accounts (account_type, customer_id) values (?,?)";

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, account.getType());
            ps.setInt(2, account.getCustomerID());
            int success = ps.executeUpdate();
            if (success > 0) {

                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        account.setAccountNum(generatedKeys.getInt(1));
                    }
                    return true;

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

}
