package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Transaction;
import java.util.List;

public interface TransactionDao {

    boolean createTransaction(Transaction transaction);
    List<Transaction> getTransactions(Account account);


}
