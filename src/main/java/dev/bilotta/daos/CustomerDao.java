package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Customer;

import java.util.List;

public interface CustomerDao {
    List<Customer> getAllCustomers();
    boolean createCustomer(Customer customer);
    List<Account> getCustomerAccounts();

}
