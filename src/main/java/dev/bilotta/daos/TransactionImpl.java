package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Transaction;
import dev.bilotta.util.ConnectionUtil;

import java.sql.*;
import java.util.*;

import java.util.List;

public class TransactionImpl implements TransactionDao{

    @Override
    public boolean createTransaction(Transaction transaction) {

        String sql = "insert into transactions (account_number, trans_type, adjustment) values (?,?,?)";

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, transaction.getAccountNum());
            ps.setString(2, transaction.getTransType());
            ps.setDouble(3, transaction.getAdjustment());
            int success = ps.executeUpdate();
            if (success > 0) {

                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        transaction.setId(generatedKeys.getInt(1));
                    }
                    return true;

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;

    }

    @Override
    public List<Transaction> getTransactions(Account account) {
        return null;
    }

}
