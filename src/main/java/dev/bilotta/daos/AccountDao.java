package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Customer;

import java.util.List;

public interface AccountDao {

    List<Account> getAllAccounts();
    boolean addNewAccount(Account account);


}
