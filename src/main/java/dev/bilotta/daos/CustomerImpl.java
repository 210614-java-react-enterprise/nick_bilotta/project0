package dev.bilotta.daos;

import dev.bilotta.models.Account;
import dev.bilotta.models.Customer;
import dev.bilotta.util.ConnectionUtil;

import java.sql.*;
import java.util.List;

public class CustomerImpl implements CustomerDao{

    @Override
    public List<Customer> getAllCustomers() {
        return null;
    }

    @Override
    public boolean createCustomer(Customer customer) {

        String sql = "insert into customers (email, password, name) values (?,?,?)";

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1,customer.getEmail());
            ps.setString(2,customer.getPassword());
            ps.setString(3,customer.getName());
            int success = ps.executeUpdate();
            if(success>0){

                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        customer.setId(generatedKeys.getInt(1));
                    }
                    return true;

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Account> getCustomerAccounts() {

        return null;
    }
}
