package dev.bilotta.util;

import java.sql.*;

public class ConnectionUtil {

    private static Connection connection;
    private static final boolean IS_TEST = Boolean.parseBoolean(System.getenv("TEST"));

    public static Connection getConnection() throws SQLException {

        if(connection==null || connection.isClosed()) {
            if (IS_TEST) {
                //connect to H2 database
                connection = DriverManager.getConnection("jdbc:h2:~/test");
            } else {
                //connect to actual databse
                // jdbc connection string format -   jdbc:postgresql://host:port/database?currentSchema=(desiredSchema)
                String url = "jdbc:postgresql://trainingdatabase.cp2fk03jqwkt.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project0";
                final String password =  System.getenv("PASSWORD");
                final String username = System.getenv("USERNAME");

                connection = DriverManager.getConnection(url, username, password);
            }
        }
        return connection;
    }

}
