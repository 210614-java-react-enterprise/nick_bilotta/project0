package dev.bilotta.util;

import dev.bilotta.daos.AccountImpl;
import dev.bilotta.daos.CustomerImpl;
import dev.bilotta.daos.TransactionImpl;
import dev.bilotta.models.Account;
import dev.bilotta.models.Customer;
import dev.bilotta.models.Transaction;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

    public Menu() {
        super();
    }

    public void start() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to Bilotta Bank! Please enter the number of a corresponding sign-in option:" +
                "\n1. Sign in to an existing customer account" +
                "\n2. Register as a new customer");

        String input = sc.nextLine();
        int choice = 0;
        boolean valid = false;

        while (!valid) {
            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException e)
            {
                System.out.println("Invalid input, please enter a number");
                input = sc.nextLine();
            }

            switch(choice) {
                case 1:
                    valid = true;
                    //Sign in to an existing account
                    login();

                    break;
                case 2:
                    //Create new account
                    valid = true;

                    startRegister();
                    break;
                default:
                    System.out.println("Number does not match a valid menu option.");
                    input = sc.nextLine();
            }
        }


    }

    public void login() {

        String sql = "select customer_id, email, password, name from customers where email = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            //Take user email address
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter the email associated with this account:");
            String emailInput = sc.nextLine();

            //Check for existing customer with that email address
            ps.setString(1, emailInput);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                //This will evaluate to true if a customer with that email address is found
                int attemptID = rs.getInt("customer_id");
                String attemptPassword = rs.getString("password");
                String attemptName = rs.getString("name");
                //Store the values of the customer account to verify login

                System.out.println("Please enter your password:");
                String passInput = sc.nextLine();
                if (passInput.equals(attemptPassword)) {
                    //This code will open the customer account page, display accounts or prompt the user to open one
                    System.out.println("Welcome "+attemptName);
                    customerAccountList(attemptID);

                } else {
                    System.out.println("The password you entered was incorrect, please try again.");
                    login();
                }

            } else {
                System.out.println("The email address given was not found, please try again.");
                login();
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    //After validating login credentials, this method calls up a list of accounts linked to the user
    public void customerAccountList(int customerID) {
        System.out.println("\nPreparing list of banking accounts...\n");

        String sql = "select * from accounts where customer_id = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            //Pull up account list for the user
            ps.setInt(1, customerID);
            ResultSet rs = ps.executeQuery();

            //Store the accountNum of associated accounts in a list
            ArrayList<Integer> userAccounts = new ArrayList<>();
            while (rs.next()) {
                userAccounts.add(rs.getInt("account_number"));
                int accountNum = rs.getInt("account_number");
                String accountType = rs.getString("account_type");
                float rawBalance = rs.getFloat("balance");
                BigDecimal balance = new BigDecimal(rawBalance);

                System.out.println("Account Number: "+accountNum+" | Type: "+accountType+" | Balance: $"+balance);
            }

            //If the account list does not populate, prompt user to create a bank account
            if (userAccounts.isEmpty()) {
                System.out.println("It looks like you have not yet opened a bank account, please open a new account below...");
                //Call to method for opening new bank account
                startNewAccount(customerID);
            }
            //Call to method that prompts account interaction (open new, close account, initiate transaction)
            accountActions(customerID, userAccounts);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void startRegister() {
    //Code for parsing inputs and prepping to call the registerUser method

    //Establish connection to databse
    String sql = "select customer_id from customers where email = ?";
    try (Connection connection = ConnectionUtil.getConnection();
         PreparedStatement ps = connection.prepareStatement(sql)) {
        //Take user email address
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the email address you would like to register with:");
        String emailInput = sc.nextLine();
        boolean valid = false;
        String regex = "^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$";

        while (!valid) {
            if (!emailInput.matches(regex)) {
                System.out.println("Invalid format for email address, please try again:");
                emailInput = sc.nextLine();
            } else if (emailInput.matches(regex)) {
                valid = true;
            }
        }

        //Check for existing customer with that email address
        ps.setString(1, emailInput);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            //if rs populates, then the email is already on record
            System.out.println("Email address already registered, would you like to:" +
                    "\n(1) sign-in or" +
                    "\n(2) create a new customer account?");
            //Logic for moving menu windows based on response
            String input = sc.nextLine();
            int choice = 0;
            valid = false;
            while (!valid) {
                try {
                    choice = Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input, please enter a number");
                    input = sc.nextLine();
                }

                switch (choice) {
                    case 1:
                        valid = true;
                        //Sign in to an existing account
                        login();

                        break;
                    case 2:
                        //Create new account
                        valid = true;

                        startRegister();
                        break;
                    default:
                        System.out.println("Number does not match a valid menu option.");
                        input = sc.nextLine();
                }
            }


        } else {
            //Take password and name inputs then call method to create new user
            System.out.println("Enter a password to use with this account:");
            String passwordInput = sc.nextLine();

            //General input validation
            valid = false;
            while (!valid) {
                if (passwordInput==null) {
                    System.out.println("Field cannot be empty");
                    passwordInput = sc.nextLine();
                } else if (passwordInput.length()>64) {
                    System.out.println("Maximum supported password length is 64 characters");
                    passwordInput = sc.nextLine();
                } else {
                    valid = true;
                }
            }
            //At this point the password should be validated, reset the valid variable
            valid = false;

            //Take use input for name
            System.out.println("Enter your name:");
            String nameInput = sc.nextLine();

            //validate the name input to be only letters and spaces, no null values
            while (!valid) {
                if (nameInput == null) {
                    System.out.println("Field cannot be empty");
                    nameInput = sc.nextLine();
                }

                Pattern p = Pattern.compile("[ A-Za-z]+$");
                Matcher m = p.matcher(nameInput);
                if (!m.matches()) {
                    System.out.println("Name must only contain letters and spaces");
                    nameInput = sc.nextLine();
                }

                valid = true;
            }

            //Inputs have been validated, should now be used to call the create method
            registerUser(emailInput, passwordInput, nameInput);
            System.out.println("Welcome to Bilotta Bank, "+nameInput);

        }
        //close the scanner object
        sc.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        //This section will call the method to create an account under the customer name

    }

    //Menu for choosing how to interact with accounts from the summary page (open new, close account, initiate transaction)
    public void accountActions(int customerID, List<Integer> accountList) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please choose an option for interacting with your accounts" +
                "\n1. Open new bank account" +
                "\n2. Make a Withdrawal"+
                "\n3. Make a Deposit"+
                "\n4. Transfer Funds"+
                "\n5. View Transaction History"+
                "\n6. Sign out" +
                "\n7. Close Program");

        String input = sc.nextLine();
        int choice = 0;
        boolean valid = false;

        //LOOP FOR VALIDATING MENU SELECTION
        while (!valid) {
            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException e)
            {
                System.out.println("Invalid input, please enter a number");
                input = sc.nextLine();
            }

            //SWITCH BLOCK TO EXECUTE MENU CHOICE
            switch(choice) {
                case 1:
                    valid = true;
                    //EXECUTE OPTION 1 - OPEN ACCOUNT
                    startNewAccount(customerID);
                    //Implementation working as intended
                    break;

                case 2:
                    //EXECUTE OPTION 3 - WITHDRAWALS
                    valid = true;

                    System.out.println("Please enter the number of the account you would like to withdraw from:");
                    String accountInput = sc.nextLine();

                    int accountChoice = 0;
                    try {
                        accountChoice = Integer.parseInt(accountInput);
                    }
                    catch (NumberFormatException e)
                    {
                        System.out.println("Invalid input, please enter a number");
                        input = sc.nextLine();
                    }

                    if (accountChoice>0) {
                        if (accountList.contains(accountChoice)) {
                            startTransaction(customerID, accountChoice, "Withdrawal");
                        } else {
                            System.out.println("Invalid account number, returning to top.");
                            accountActions(customerID, accountList);
                        }
                    } else {
                        System.out.println("There was a problem selecting your account");
                    }
                    break;

                case 3:
                    //EXECUTE OPTION 4 - DEPOSITS
                    valid = true;

                    System.out.println("Please enter the number of the account you would like to deposit into:");
                    accountInput = sc.nextLine();

                    accountChoice = 0;
                    try {
                        accountChoice = Integer.parseInt(accountInput);
                    }
                    catch (NumberFormatException e)
                    {
                        System.out.println("Invalid input, please enter a number");
                        input = sc.nextLine();
                    }

                    if (accountChoice>0) {
                        if (accountList.contains(accountChoice)) {
                            startTransaction(customerID, accountChoice, "Deposit");
                        } else {
                            System.out.println("Invalid account number, returning to top.");
                            accountActions(customerID, accountList);
                        }
                    } else {
                        System.out.println("There was a problem selecting your account");
                    }
                    break;

                case 4:
                    //EXECUTE OPTION 5 - TRANSFER
                    valid = true;
                    newTransfer(customerID, accountList);

                    break;

                case 5:
                    //Pull up transaction history for one account, validate account ownership here and pass acc#
                    //Validate selection here, only pass acc# to method
                    System.out.println("Enter the account number for a history summary");
                    String historyInput = sc.nextLine();

                    int historyChoice = 0;
                    valid = false;

                    //LOOP FOR VALIDATING MENU SELECTION
                    while (!valid) {
                        try {
                            historyChoice = Integer.parseInt(historyInput);
                        } catch (NumberFormatException e) {
                            System.out.println("Invalid input, please enter a number");
                            historyInput = sc.nextLine();
                        }
                        if (accountList.contains(historyChoice)) {
                            //Input validated and exists in customer account
                            valid = true;
                            transactionHistory(customerID, historyChoice);
                        } else {
                            System.out.println("The account number does not match your records, try again:");
                            historyInput = sc.nextLine();
                        }
                    }
                    break;

                case 6:
                    //Return to start menu
                    valid = true;
                    start();
                    break;

                case 7:
                    //Close the application
                    valid = true;
                    System.out.println("Ending session...");
                    break;

                default:
                    System.out.println("Number does not match a valid menu option.");
                    input = sc.nextLine();
            }
        }
    }

    public void startNewAccount(int customerID) {
        //Menu path for creating new bank account under a signed-in user
        Scanner sc = new Scanner(System.in);
        System.out.println("Please select the type of account you would like to open:" +
                "\n1. Checking Account" +
                "\n2. Savings Account");

        String input = sc.nextLine();
        int choice = 0;
        boolean valid = false;

        //VALIDATING MENU SELECTION
        while (!valid) {
            try {
                choice = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input, please enter a number");
                input = sc.nextLine();
            }

            //Check the account type input, catch invalid selections
            if (choice == 1) {
                valid = true;
                openNewAccount("Checking", customerID);
                //call the method to create an account of this type, passing the customerID
            } else if (choice == 2) {
                valid = true;
                openNewAccount("Savings", customerID);
            } else {
                System.out.println("Invalid input, please enter a number from the menu.");
                input = sc.nextLine();
            }
        }

    }


    public void startTransaction(int customerID, int accountNum, String type) {

        String sql = "select balance from accounts where account_number = ?";
        //Connect to database
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, accountNum);
            ResultSet rs = ps.executeQuery();
            float startBalance = 0;

            while (rs.next()) {
                startBalance = rs.getFloat("balance");
            }


            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the amount of this "+type+" (enter amount in dollars format ###.##)");
            String input = sc.nextLine();

            if (input.length()>16) {
                System.out.println("Maximum supported input is 16 digits, please retry.");
                input = sc.nextLine();
            }

            float inputAmount = 0;
            boolean valid = false;
            while (!valid) {
                try {
                    inputAmount = Float.parseFloat(input);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input, please enter a number");
                    input = sc.nextLine();
                }
                if (inputAmount<=0) {
                    System.out.println("Input must be a positive number greater than zero, try again");
                    input = sc.nextLine();
                } else if (inputAmount>0) {
                    valid = true;
                }
            }

            //Formatting pattern to round float inputs to the 2nd decimal place
            DecimalFormat df = new DecimalFormat("#.##");

            //Take user input on adjustment amount
            switch (type) {
                case "Withdrawal":
                    //Logic for deducting from account balance

                    if (inputAmount>startBalance) {
                        System.out.println("Withdrawal amount cannot exceed available balance");
                        customerAccountList(customerID);
                    } else if (inputAmount<=startBalance) {
                        //Logic here for creating transaction event and then adjusting the account balance
                        //Rounding input to 2 decimals and formatting for use in sql statement

                        String roundedInput = df.format(inputAmount);

                        float roundedFloat = Float.parseFloat(roundedInput);
                        float newBal = startBalance-roundedFloat;

                        String finalAdjustment = df.format(newBal);
                        float adjustment = Float.parseFloat(finalAdjustment);

                        //update the balance in the account
                        String sqlWithdraw = "update accounts set balance = ? where account_number = ?";
                        try (PreparedStatement updateBal = connection.prepareStatement(sqlWithdraw)) {
                            //Statement calls for DB
                            updateBal.setFloat(1, adjustment);
                            updateBal.setInt(2, accountNum);
                            int success = updateBal.executeUpdate();

                            if (success>0) {
                                newTransaction(customerID, accountNum, "Withdraw", -roundedFloat);
                                System.out.println("Transaction successful, returning to summary page.");
                                customerAccountList(customerID);
                            }

                        }

                    } else {
                        System.out.println("There was an issue processing this transaction");
                    }
                    break;

                case "Deposit":
                    //Add amount to account balance

                    String roundedInput = df.format(inputAmount);

                    float roundedFloat = Float.parseFloat(roundedInput);
                    float newBal = startBalance+roundedFloat;

                    String finalAdjustment = df.format(newBal);
                    float adjustment = Float.parseFloat(finalAdjustment);

                    //update the balance in the account
                    String sqlDeposit = "update accounts set balance = ? where account_number = ?";
                    try (PreparedStatement updateBal = connection.prepareStatement(sqlDeposit)) {
                        //Statement calls for DB
                        updateBal.setFloat(1, adjustment);
                        updateBal.setInt(2, accountNum);
                        int success = updateBal.executeUpdate();

                        if (success>0) {
                            newTransaction(customerID, accountNum, "Deposit", roundedFloat);
                            System.out.println("Transaction successful, returning to summary page.");
                            customerAccountList(customerID);
                        }

                    }

                    break;

                default:
                    System.out.println("There was an issue initiating this transaction");
            }

            } catch (SQLException throwables) {
            throwables.printStackTrace();
            }

    }

    public void newTransfer(int customerID, List<Integer> accountList) {
        //Transfers will have to validate both accounts before deducting/adding to both accounts
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the account number you will be WITHDRAWING from:");

        boolean valid = false;
        String withdrawAcc = sc.nextLine();
        int withdrawAccChoice = 0;
        int depositAccChoice = 0;

        //Loop to validate that the input is indeed an integer
        while (!valid) {
            try {
                withdrawAccChoice = Integer.parseInt(withdrawAcc);

            } catch (NumberFormatException e) {
                System.out.println("Invalid input, please enter a number");
                withdrawAcc = sc.nextLine();
            }

            if (withdrawAccChoice>0) {
                //Check to see if the account number is in the list of accounts for this customer
                if (accountList.contains(withdrawAccChoice)) {
                    //Code for selecting and checking the destination account
                    System.out.println("Enter the number for the destination account:");
                    String depositAcc = sc.nextLine();

                    while (!valid) {
                        try {
                            depositAccChoice = Integer.parseInt(depositAcc);

                        } catch (NumberFormatException e) {
                            System.out.println("Invalid input, please enter a number");
                            depositAcc = sc.nextLine();
                        }

                        if (withdrawAccChoice==depositAccChoice) {
                            System.out.println("Target account cannot be the same as the initial account, try again.");
                            depositAcc = sc.nextLine();
                        } else if (accountList.contains(depositAccChoice)) {
                            //Validate that the deposit account is correct
                            valid = true;
                            //end the while loop
                        }
                    }


                }

            } else {
                System.out.println("There was a problem selecting your account(s)");
            }

        }

        String getBal = "select balance from accounts where account_number = ?";
        //Connect to database
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(getBal)) {
            ps.setInt(1, withdrawAccChoice);
            ResultSet rs = ps.executeQuery();
            float startBalance = 0;

            while (rs.next()) {
                startBalance = rs.getFloat("balance");
            }

            //Code for taking a transfer amount from the user
            System.out.println("Enter the amount of this transfer (enter amount in dollars format ###.##)");
            String input = sc.nextLine();

            if (input.length()>16) {
                System.out.println("Maximum supported input is 16 digits, please retry.");
                input = sc.nextLine();
            }

            //Making sure that the input is  valid integer
            float inputAmount = 0;
            valid = false;
            while (!valid) {
                try {
                    inputAmount = Float.parseFloat(input);
                    valid = true;
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input, please enter a number");
                    input = sc.nextLine();
                }
            }

            //Formatting pattern to round float inputs to the 2nd decimal place
            DecimalFormat df = new DecimalFormat("#.##");

            if (inputAmount>startBalance) {
                System.out.println("Withdrawal amount cannot exceed available balance");
            } else if (inputAmount<=startBalance) {
                //Logic here for creating transaction event and then adjusting the account balance
                //Rounding input to 2 decimals and formatting for use in sql statement
                String roundedInput = df.format(inputAmount);

                float roundedFloat = Float.parseFloat(roundedInput);
                float newBal = startBalance - roundedFloat;

                String finalAdjustment = df.format(newBal);
                float adjustment = Float.parseFloat(finalAdjustment);

                //update the balance in the first account
                String sqlWithdraw = "update accounts set balance = ? where account_number = ?";
                try (PreparedStatement updateBal = connection.prepareStatement(sqlWithdraw)) {
                    //Statement calls for DB
                    updateBal.setFloat(1, adjustment);
                    updateBal.setInt(2, withdrawAccChoice);
                    int success = updateBal.executeUpdate();

                    if (success>0) {
                        //If withdrawal succeeds, move on to the deposit

                        String sql = "select balance from accounts where account_number = ?";
                        //Connect to database
                        try (PreparedStatement getDepBal = connection.prepareStatement(sql)) {

                            getDepBal.setInt(1, depositAccChoice);
                            ResultSet rsDep = getDepBal.executeQuery();
                            startBalance = 0;

                            while (rsDep.next()) {
                                startBalance = rsDep.getFloat("balance");
                            }

                            roundedInput = df.format(inputAmount);

                            roundedFloat = Float.parseFloat(roundedInput);
                            newBal = startBalance + roundedFloat;

                            finalAdjustment = df.format(newBal);
                            adjustment = Float.parseFloat(finalAdjustment);

                            //update the balance in the account
                            String sqlDeposit = "update accounts set balance = ? where account_number = ?";
                            try (PreparedStatement updateBal2 = connection.prepareStatement(sqlDeposit)) {
                                //Statement calls for DB
                                updateBal2.setFloat(1, adjustment);
                                updateBal2.setInt(2, depositAccChoice);
                                success = updateBal2.executeUpdate();

                                if (success > 0) {
                                    newTransaction(customerID, depositAccChoice, "Transfer", roundedFloat);
                                    newTransaction(customerID, withdrawAccChoice, "Transfer", -roundedFloat);
                                    System.out.println("Transaction successful, returning to summary page.");
                                    customerAccountList(customerID);
                                }

                            }
                        }

                    }

                }


            } else {
                System.out.println("There was a problem processing this request");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void transactionHistory(int customerID, int accountNum) {
        //Pull up a list of transactions for one account
        String sql = "select trans_type, adjustment from transactions where account_number = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            //Pull up the set of transactions
            ps.setInt(1, accountNum);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                System.out.println("Type: " + rs.getString("trans_type")+" | $"+rs.getFloat("adjustment"));
            }

            customerAccountList(customerID);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


        // Function attempts create a new customer object and add its values to the DB, returns a boolean to indicate success or failure
    public void registerUser (String email, String password, String name) {
        Customer newCustomer = new Customer(email, password, name);
        CustomerImpl customerdao = new CustomerImpl();
        boolean success = customerdao.createCustomer(newCustomer);
        if (!success) {
            System.out.println("There was a problem creating your user account, please restart.");
            start();
        }
        customerAccountList(newCustomer.getId());
    }

    public void openNewAccount (String type, int customerID) {
        Account account = new Account(type, customerID);
        AccountImpl accountdao = new AccountImpl();
        boolean success = accountdao.addNewAccount(account);
        if (!success) {
            System.out.println("There was a problem creating your bank account, returning to summary page");
            customerAccountList(customerID);
        }
        customerAccountList(customerID);
    }

    public void newTransaction (int customerID, int accountNum, String transType, float adjustment) {
        Transaction transaction = new Transaction(accountNum, transType, adjustment);
        TransactionImpl transdao = new TransactionImpl();
        boolean success = transdao.createTransaction(transaction);
        if (!success) {
            System.out.println("This transaction has failed, returning to summary page");
            customerAccountList(customerID);
        }
    }


}
